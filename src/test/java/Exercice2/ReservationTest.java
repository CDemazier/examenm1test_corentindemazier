package Exercice2;

import Exercice1.CarteCreditManager;
import Exercice1.PaymentGateway;
import Exercice1.PaymentProcessor;
import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class ReservationTest {

    @Test
    public void testReservationValide() {

        //création des variables
        Medecin med = new Medecin(1);
        Patient pat = new Patient(1);
        Reservation res = new Reservation(med, pat);
        LocalDateTime date = LocalDateTime.parse("2022-04-02T13:00:00");

        //execution du code
        boolean created = res.reservation(1, 1, date);

        //assertions
        assertTrue(created);

    }

    @Test
    public void testReservationPatientInvalide() {

        //création des variables
        Medecin med = new Medecin(1);
        Patient pat = new Patient(1);
        Reservation res = new Reservation(med, pat);
        LocalDateTime date = LocalDateTime.parse("2022-04-02T13:00:00");

        //execution du code
        boolean created = res.reservation(1, 2, date);

        //assertions
        assertFalse(created);

    }

    @Test
    public void testReservationMedecinInvalide() {

        //création des variables
        Medecin med = new Medecin(1);
        Patient pat = new Patient(1);
        Reservation res = new Reservation(med, pat);
        LocalDateTime date = LocalDateTime.parse("2022-04-02T13:00:00");

        //execution du code
        boolean created = res.reservation(2, 1, date);

        //assertions
        assertFalse(created);

    }

    @Test
    public void testReservationDateIndisponible() {

        //création des variables
        Medecin med = new Medecin(1, LocalDateTime.parse("2022-04-02T13:00:00"));
        Patient pat = new Patient(1);
        Reservation res = new Reservation(med, pat);
        LocalDateTime date = LocalDateTime.parse("2022-04-02T13:00:00");

        //execution du code
        boolean created = res.reservation(1, 1, date);

        //assertions
        assertFalse(created);

    }

    @Test
    public void testReservationPatientOccupe() {

        //création des variables
        Medecin med = new Medecin(1);
        Patient pat = new Patient(1, LocalDateTime.parse("2022-04-02T13:00:00"));
        Reservation res = new Reservation(med, pat);
        LocalDateTime date = LocalDateTime.parse("2022-04-02T13:00:00");

        //execution du code
        boolean created = res.reservation(1, 1, date);

        //assertions
        assertFalse(created);

    }

    @Test
    public void testReservationMedecinOccupe() {

        //création des variables
        Medecin med = new Medecin(1, LocalDateTime.parse("2022-04-02T13:00:00"));
        Patient pat = new Patient(1);
        Reservation res = new Reservation(med, pat);
        LocalDateTime date = LocalDateTime.parse("2022-04-02T13:00:00");

        //execution du code
        boolean created = res.reservation(1, 1, date);

        //assertions
        assertFalse(created);

    }

    @ParameterizedTest
    @CsvSource({
            "2, 1, 2022-04-01T11:00",
            "1, 2, 2022-04-02T13:00:00",
            "2, 2, 2022-04-02T14:00:00",
            "3, 2, 2022-04-02T15:00:00",
            "4, 2, 2022-04-02T16:00:00"
    })
    public void testReservationParametre(int idpat, int idmed, String dat) {

        //création des variables
        Medecin med = new Medecin(idmed, LocalDateTime.parse("2022-04-02T16:00:00"));
        Patient pat = new Patient(idpat);
        Reservation res = new Reservation(med, pat);
        LocalDateTime date = LocalDateTime.parse(dat);

        //execution du code
        boolean created = res.reservation(idmed, idpat, date);

        //assertions
        assertTrue(created);

    }

}
