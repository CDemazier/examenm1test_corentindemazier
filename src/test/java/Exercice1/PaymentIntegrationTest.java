package Exercice1;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PaymentIntegrationTest {

    /*
    * Test d'intégration de paiement réussi
    * */
    @Test
    public void testSucessPayment() {

        //création des variables
        PaymentGateway pg = new PaymentGateway();
        CarteCreditManager ccm = new CarteCreditManager();
        PaymentProcessor pp = new PaymentProcessor();
        double montant = 15000;
        String cvv = "148";
        String numCart = "1478147814781478";

        //execution du code
        boolean validCard = ccm.verifyCard(numCart, cvv);
        int id = pg.makePayment(montant);
        boolean paymentMade = pp.processPayment(montant, numCart, cvv);

        //assertions
        assertTrue(validCard);
        assertTrue(1000 <= id && id <= 9999);
        assertTrue(paymentMade);

    }

    /*
     * Test d'intégration de paiement échoué due à une carte invalide
     * */
    @Test
    public void testFailedCB() {

        //création des variables
        PaymentGateway pg = new PaymentGateway();
        CarteCreditManager ccm = new CarteCreditManager();
        PaymentProcessor pp = new PaymentProcessor();
        double montant = 15000;
        String cvv = "78";
        String numCart = "79659478";

        //execution du code
        boolean validCard = ccm.verifyCard(numCart, cvv);
        int id = pg.makePayment(montant);
        boolean paymentMade = pp.processPayment(montant, numCart, cvv);

        //assertions
        assertFalse(validCard);
        assertTrue(1000 <= id && id <= 9999);
        assertFalse(paymentMade);

    }

}
