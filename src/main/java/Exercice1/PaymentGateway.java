package Exercice1;

public class PaymentGateway {

    public int makePayment(double montant) {
        if (montant <= 20000) {
            return generateRandom();
        }
        else {
            return 0;
        }
    }

    public static int generateRandom() {
        int min = 1000; // Minimum value of range
        int max = 9999; // Maximum value of range
        // Generate random int value from min to max
        int random_int = (int)Math.floor(Math.random() * (max - min + 1) + min);
        // Printing the generated random numbers
        return random_int;
    }

}
