package Exercice1;

public class PaymentProcessor {

    public boolean processPayment(double montant, String numCart, String cvv) {
        CarteCreditManager ccm = new CarteCreditManager();
        if(ccm.verifyCard(numCart, cvv)) {
            PaymentGateway pg = new PaymentGateway();
            return pg.makePayment(montant) != 0;
        }
        else {
            return false;
        }
    }

}
