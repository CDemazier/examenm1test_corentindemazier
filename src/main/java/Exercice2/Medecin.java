package Exercice2;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Medecin {

    int idMedecin;
    List<LocalDateTime> listeReseved = new ArrayList<LocalDateTime>();

    public Medecin(int id) {
        this.idMedecin = id;
    }

    public Medecin(int id, LocalDateTime date) {
        this.idMedecin = id;
        this.listeReseved.add(date);
    }

    public boolean reserver(LocalDateTime date) {

        if(isDispo(date)) {
            this.listeReseved.add(date);
            return true;
        }
        else {
            return false;
        }

    }

    public boolean isDispo(LocalDateTime date) {

        return !listeReseved.contains(date);

    }

}
