package Exercice2;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Reservation {

    Medecin Medecin;
    Patient Patient;
    LocalDateTime date;

    public Reservation(Medecin med, Patient pat) {
        this.Medecin = med;
        this.Patient = pat;
    }

    public boolean reservation(int idMed, int idPat, LocalDateTime dat) {
        if (this.Medecin.idMedecin == idMed && this.Patient.idPatient == idPat) {
            if (this.Medecin.reserver(dat) && this.Patient.reserver(dat)) {
                this.date = dat;
                return true;
            } else {
                return false;
            }
        }
        else {
            return false;
        }

    }

}
